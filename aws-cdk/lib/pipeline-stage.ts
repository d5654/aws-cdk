import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as s3 from 'aws-cdk-lib/aws-s3';
import { LambdaStack } from './lambda-stack';


export interface LambdaAppStageProps extends cdk.StageProps {
  serviceName: string;
  s3Bucket: s3.IBucket;
  snsTopicArn: string;
}

export class LambdaAppStage extends cdk.Stage {
  constructor(scope: Construct, id: string, props: LambdaAppStageProps) {
    super(scope, id, props);

    new LambdaStack(this, 'lambdaStack', {
      serviceName: props.serviceName,
      s3Bucket: props.s3Bucket,
      snsTopicArn: props.snsTopicArn,
    })
  }
}