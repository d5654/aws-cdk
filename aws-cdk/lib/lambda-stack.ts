import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as iam from 'aws-cdk-lib/aws-iam';

export interface LambdaStackProps extends cdk.StackProps {
    serviceName: string;
    s3Bucket: s3.IBucket;
    snsTopicArn: string;
}

export class LambdaStack extends cdk.Stack {
    constructor(scope: Construct, id: string, props: LambdaStackProps) {
        super(scope, id, props);

    const cdkSmsLambdaFunction = new lambda.Function(this, 'cdkSmsLambdaFunction', {
        functionName: "cdk-send-sms",
        handler: "main",
        memorySize: 128,
        runtime: lambda.Runtime.GO_1_X,
        timeout: cdk.Duration.seconds(5),
        code: lambda.Code.fromBucket(props.s3Bucket, 'function.zip'),
        environment: {
            SNS_TOPIC_ARN: props.snsTopicArn,
        }
    });

    const policy = new iam.PolicyStatement({
        actions: ['sns:Publish'],
        resources: [props.snsTopicArn],
    });

    cdkSmsLambdaFunction.addToRolePolicy(policy);

  }
}