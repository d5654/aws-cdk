import { App, Stack } from 'aws-cdk-lib';
import { Template } from 'aws-cdk-lib/assertions';
import { PipelineStack } from '../lib/pipeline-stack'; 


const serviceName = "test-pipeline"
const env = {
    account: '01234567890',
    region: 'eu-west-1',
  };

describe('The Pipeline Stack', () => {
    const app = new App();
    const stack = new PipelineStack(app, 'pipelineTestStack', {
      serviceName,
      env,
    });
  
    it('should match snapshot', () => {
      const template = Template.fromStack(stack);
      expect(template).toMatchSnapshot();
    });
  });
