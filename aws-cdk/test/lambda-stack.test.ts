import { App, Stack } from 'aws-cdk-lib';
import { Template } from 'aws-cdk-lib/assertions';
import * as s3 from 'aws-cdk-lib/aws-s3';
import { LambdaStack } from '../lib/lambda-stack';


const serviceName = "test-pipeline"

describe('The Lambda Stack', () => {
    const app = new App();
    const bucketStack = new Stack(app, "bucketStack");
    const s3Bucket = new s3.Bucket(bucketStack, 's3Bucket');
    const snsTopicArn = 'dummy-topic-arn';

    const stack = new LambdaStack(app, 'lambdaTestStack', {
        s3Bucket,
        serviceName,
        snsTopicArn,
    });

    it('should match snapshot', () => {
        const template = Template.fromStack(stack);
        expect(template).toMatchSnapshot();
    });
});